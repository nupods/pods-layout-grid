<?php
/**
 * Plugin Name: PODS Layout Grid
 * Description: Add a filter to the_content to create a grid using ACF
 */

namespace PODS\LayoutGrid;

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}


if (!class_exists('LayoutGrid')) {
    class LayoutGrid
    {

        // vars
        var $settings;


        /*
        *  __construct
        *
        *  A dummy constructor to ensure initialized once
        */

        function __construct() {

            /* Do nothing here */

        }


        /*
        *  initialize
        *
        *  The real constructor to initialize ACF
        *
        */

        public function initialize()
        {

            // vars
            $this->settings = array(
                // basic
                'name'              => __('PODS LayoutGrid'),
                'version'           => '0.1',

                // urls
                'basename'          => plugin_basename( __FILE__ ),
                'path'              => plugin_dir_path( __FILE__ ),
                'dir'               => plugin_dir_url( __FILE__ ),
            );

            // actions
            add_action('init',  array($this, 'defineFields'), 20);

            // filters
            add_filter('the_content', array($this, 'contentFilter'), 10, 2);

        }


        /*
        *  defineFields
        *
        *  Defines ACF's
        *
        *  @param   N/A
        *  @return  N/A
        */

        public function defineFields()
        {
            // Loop through potential locations
            $locations = [];

            // Add Options Homepage
            $locations[] = array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-homepage'
                )
            );
            foreach (get_post_types(['public' => true]) as $post_type) {
                $locations[] = array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => $post_type
                    )
                );
            }

            // Flex Layout
            if (function_exists('acf_add_local_field_group')) {
                acf_add_local_field_group(array (
                    'key' => 'group_55412ccfd977c',
                    'title' => '[layout] Flexible Layout',
                    'fields' => array (
                        array (
                            'key' => 'field_55412cd5eece1',
                            'label' => 'Flexible Layout',
                            'name' => 'flex_layout',
                            'type' => 'flexible_content',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => 'field_type-flexible_content field_key-field_55412cd5eece1',
                                'id' => '',
                            ),
                            'button_label' => 'Add a Layout',
                            'min' => '',
                            'max' => '',
                            'layouts' => array (
                                array (
                                    'key' => '55412cdc8061b',
                                    'name' => 'grid',
                                    'label' => 'Grid Row',
                                    'display' => 'row',
                                    'sub_fields' => array (
                                        array (
                                            'key' => 'field_566b2567445a7',
                                            'label' => 'Layout Container',
                                            'name' => 'select_container',
                                            'type' => 'select',
                                            'instructions' => '',
                                            'required' => 0,
                                            'conditional_logic' => 0,
                                            'wrapper' => array (
                                                'width' => '',
                                                'class' => 'field_type-text field_key-field_55b2642140706',
                                                'id' => '',
                                            ),
                                            'choices' => array (
                                                'container-none' => 'None',
                                                'container' => 'Basic',
                                                'container-fluid' => 'Fluid',
                                            ),
                                            'default_value' => array (
                                                'None' => 'None',
                                            ),
                                            'allow_null' => 0,
                                            'multiple' => 0,
                                            'ui' => 0,
                                            'ajax' => 0,
                                            'placeholder' => '',
                                            'disabled' => 0,
                                            'readonly' => 0,
                                        ),
                                        array (
                                            'key' => 'field_566b0a5fe26ce',
                                            'label' => 'Layout ID',
                                            'name' => 'text_id',
                                            'type' => 'text',
                                            'instructions' => '',
                                            'required' => 0,
                                            'conditional_logic' => 0,
                                            'wrapper' => array (
                                                'width' => '',
                                                'class' => 'field_type-text field_key-field_55b2642140706',
                                                'id' => '',
                                            ),
                                            'default_value' => '',
                                            'placeholder' => 'lowercase-id',
                                            'prepend' => '',
                                            'append' => '',
                                            'maxlength' => '',
                                            'readonly' => 0,
                                            'disabled' => 0,
                                        ),
                                        array (
                                            'key' => 'field_55b2642140706',
                                            'label' => 'Layout Class',
                                            'name' => 'text_class',
                                            'type' => 'text',
                                            'instructions' => '',
                                            'required' => 0,
                                            'conditional_logic' => 0,
                                            'wrapper' => array (
                                                'width' => '',
                                                'class' => 'field_type-text field_key-field_55b2642140706',
                                                'id' => '',
                                            ),
                                            'default_value' => '',
                                            'placeholder' => 'css-class',
                                            'prepend' => '',
                                            'append' => '',
                                            'maxlength' => '',
                                            'readonly' => 0,
                                            'disabled' => 0,
                                        ),
                                        array (
                                            'key' => 'field_55412d0810d4e',
                                            'label' => 'Add a Grid',
                                            'name' => 'repeater_grid',
                                            'type' => 'repeater',
                                            'instructions' => '',
                                            'required' => 0,
                                            'conditional_logic' => 0,
                                            'wrapper' => array (
                                                'width' => '',
                                                'class' => 'field_type-repeater field_key-field_55412d0810d4e',
                                                'id' => '',
                                            ),
                                            'collapsed' => 'field_55412de6aa3ad',
                                            'min' => '',
                                            'max' => '',
                                            'layout' => 'row',
                                            'button_label' => 'Add Grid Item',
                                            'sub_fields' => array (
                                                array (
                                                    'key' => 'field_55412de6aa3ad',
                                                    'label' => 'Columns',
                                                    'name' => 'select_columns',
                                                    'type' => 'select',
                                                    'instructions' => '',
                                                    'required' => 0,
                                                    'conditional_logic' => 0,
                                                    'wrapper' => array (
                                                        'width' => '',
                                                        'class' => 'field_type-select field_key-field_55412de6aa3ad',
                                                        'id' => '',
                                                    ),
                                                    'choices' => array (
                                                        'xs-1' => 'xs-1',
                                                        'sm-1' => 'sm-1',
                                                        'md-1' => 'md-1',
                                                        'lg-1' => 'lg-1',
                                                        'xs-2' => 'xs-2',
                                                        'sm-2' => 'sm-2',
                                                        'md-2' => 'md-2',
                                                        'lg-2' => 'lg-2',
                                                        'xs-3' => 'xs-3',
                                                        'sm-3' => 'sm-3',
                                                        'md-3' => 'md-3',
                                                        'lg-3' => 'lg-3',
                                                        'xs-4' => 'xs-4',
                                                        'sm-4' => 'sm-4',
                                                        'md-4' => 'md-4',
                                                        'lg-4' => 'lg-4',
                                                        'xs-5' => 'xs-5',
                                                        'sm-5' => 'sm-5',
                                                        'md-5' => 'md-5',
                                                        'lg-5' => 'lg-5',
                                                        'xs-6' => 'xs-6',
                                                        'sm-6' => 'sm-6',
                                                        'md-6' => 'md-6',
                                                        'lg-6' => 'lg-6',
                                                        'xs-7' => 'xs-7',
                                                        'sm-7' => 'sm-7',
                                                        'md-7' => 'md-7',
                                                        'lg-7' => 'lg-7',
                                                        'xs-8' => 'xs-8',
                                                        'sm-8' => 'sm-8',
                                                        'md-8' => 'md-8',
                                                        'lg-8' => 'lg-8',
                                                        'xs-9' => 'xs-9',
                                                        'sm-9' => 'sm-9',
                                                        'md-9' => 'md-9',
                                                        'lg-9' => 'lg-9',
                                                        'xs-10' => 'xs-10',
                                                        'sm-10' => 'sm-10',
                                                        'md-10' => 'md-10',
                                                        'lg-10' => 'lg-10',
                                                        'xs-11' => 'xs-11',
                                                        'sm-11' => 'sm-11',
                                                        'md-11' => 'md-11',
                                                        'lg-11' => 'lg-11',
                                                        'xs-12' => 'xs-12',
                                                        'sm-12' => 'sm-12',
                                                        'md-12' => 'md-12',
                                                        'lg-12' => 'lg-12',
                                                        'xs-pull-0' => 'xs-pull-0',
                                                        'xs-pull-1' => 'xs-pull-1',
                                                        'xs-pull-2' => 'xs-pull-2',
                                                        'xs-pull-3' => 'xs-pull-3',
                                                        'xs-pull-4' => 'xs-pull-4',
                                                        'xs-pull-5' => 'xs-pull-5',
                                                        'xs-pull-6' => 'xs-pull-6',
                                                        'xs-pull-7' => 'xs-pull-7',
                                                        'xs-pull-8' => 'xs-pull-8',
                                                        'xs-pull-9' => 'xs-pull-9',
                                                        'xs-pull-10' => 'xs-pull-10',
                                                        'xs-pull-11' => 'xs-pull-11',
                                                        'xs-pull-12' => 'xs-pull-12',
                                                        'sm-pull-0' => 'sm-pull-0',
                                                        'sm-pull-1' => 'sm-pull-1',
                                                        'sm-pull-2' => 'sm-pull-2',
                                                        'sm-pull-3' => 'sm-pull-3',
                                                        'sm-pull-4' => 'sm-pull-4',
                                                        'sm-pull-5' => 'sm-pull-5',
                                                        'sm-pull-6' => 'sm-pull-6',
                                                        'sm-pull-7' => 'sm-pull-7',
                                                        'sm-pull-8' => 'sm-pull-8',
                                                        'sm-pull-9' => 'sm-pull-9',
                                                        'sm-pull-10' => 'sm-pull-10',
                                                        'sm-pull-11' => 'sm-pull-11',
                                                        'sm-pull-12' => 'sm-pull-12',
                                                        'md-pull-0' => 'md-pull-0',
                                                        'md-pull-1' => 'md-pull-1',
                                                        'md-pull-2' => 'md-pull-2',
                                                        'md-pull-3' => 'md-pull-3',
                                                        'md-pull-4' => 'md-pull-4',
                                                        'md-pull-5' => 'md-pull-5',
                                                        'md-pull-6' => 'md-pull-6',
                                                        'md-pull-7' => 'md-pull-7',
                                                        'md-pull-8' => 'md-pull-8',
                                                        'md-pull-9' => 'md-pull-9',
                                                        'md-pull-10' => 'md-pull-10',
                                                        'md-pull-11' => 'md-pull-11',
                                                        'md-pull-12' => 'md-pull-12',
                                                        'lg-pull-0' => 'lg-pull-0',
                                                        'lg-pull-1' => 'lg-pull-1',
                                                        'lg-pull-2' => 'lg-pull-2',
                                                        'lg-pull-3' => 'lg-pull-3',
                                                        'lg-pull-4' => 'lg-pull-4',
                                                        'lg-pull-5' => 'lg-pull-5',
                                                        'lg-pull-6' => 'lg-pull-6',
                                                        'lg-pull-7' => 'lg-pull-7',
                                                        'lg-pull-8' => 'lg-pull-8',
                                                        'lg-pull-9' => 'lg-pull-9',
                                                        'lg-pull-10' => 'lg-pull-10',
                                                        'lg-pull-11' => 'lg-pull-11',
                                                        'lg-pull-12' => 'lg-pull-12',
                                                        'xs-push-0' => 'xs-push-0',
                                                        'xs-push-1' => 'xs-push-1',
                                                        'xs-push-2' => 'xs-push-2',
                                                        'xs-push-3' => 'xs-push-3',
                                                        'xs-push-4' => 'xs-push-4',
                                                        'xs-push-5' => 'xs-push-5',
                                                        'xs-push-6' => 'xs-push-6',
                                                        'xs-push-7' => 'xs-push-7',
                                                        'xs-push-8' => 'xs-push-8',
                                                        'xs-push-9' => 'xs-push-9',
                                                        'xs-push-10' => 'xs-push-10',
                                                        'xs-push-11' => 'xs-push-11',
                                                        'xs-push-12' => 'xs-push-12',
                                                        'sm-push-0' => 'sm-push-0',
                                                        'sm-push-1' => 'sm-push-1',
                                                        'sm-push-2' => 'sm-push-2',
                                                        'sm-push-3' => 'sm-push-3',
                                                        'sm-push-4' => 'sm-push-4',
                                                        'sm-push-5' => 'sm-push-5',
                                                        'sm-push-6' => 'sm-push-6',
                                                        'sm-push-7' => 'sm-push-7',
                                                        'sm-push-8' => 'sm-push-8',
                                                        'sm-push-9' => 'sm-push-9',
                                                        'sm-push-10' => 'sm-push-10',
                                                        'sm-push-11' => 'sm-push-11',
                                                        'sm-push-12' => 'sm-push-12',
                                                        'md-push-0' => 'md-push-0',
                                                        'md-push-1' => 'md-push-1',
                                                        'md-push-2' => 'md-push-2',
                                                        'md-push-3' => 'md-push-3',
                                                        'md-push-4' => 'md-push-4',
                                                        'md-push-5' => 'md-push-5',
                                                        'md-push-6' => 'md-push-6',
                                                        'md-push-7' => 'md-push-7',
                                                        'md-push-8' => 'md-push-8',
                                                        'md-push-9' => 'md-push-9',
                                                        'md-push-10' => 'md-push-10',
                                                        'md-push-11' => 'md-push-11',
                                                        'md-push-12' => 'md-push-12',
                                                        'lg-push-0' => 'lg-push-0',
                                                        'lg-push-1' => 'lg-push-1',
                                                        'lg-push-2' => 'lg-push-2',
                                                        'lg-push-3' => 'lg-push-3',
                                                        'lg-push-4' => 'lg-push-4',
                                                        'lg-push-5' => 'lg-push-5',
                                                        'lg-push-6' => 'lg-push-6',
                                                        'lg-push-7' => 'lg-push-7',
                                                        'lg-push-8' => 'lg-push-8',
                                                        'lg-push-9' => 'lg-push-9',
                                                        'lg-push-10' => 'lg-push-10',
                                                        'lg-push-11' => 'lg-push-11',
                                                        'lg-push-12' => 'lg-push-12',
                                                        'xs-offset-1' => 'xs-offset-1',
                                                        'xs-offset-2' => 'xs-offset-2',
                                                        'xs-offset-3' => 'xs-offset-3',
                                                        'xs-offset-4' => 'xs-offset-4',
                                                        'xs-offset-5' => 'xs-offset-5',
                                                        'xs-offset-6' => 'xs-offset-6',
                                                        'xs-offset-7' => 'xs-offset-7',
                                                        'xs-offset-8' => 'xs-offset-8',
                                                        'xs-offset-9' => 'xs-offset-9',
                                                        'xs-offset-10' => 'xs-offset-10',
                                                        'xs-offset-11' => 'xs-offset-11',
                                                        'xs-offset-12' => 'xs-offset-12',
                                                        'sm-offset-0' => 'sm-offset-0',
                                                        'sm-offset-1' => 'sm-offset-1',
                                                        'sm-offset-2' => 'sm-offset-2',
                                                        'sm-offset-3' => 'sm-offset-3',
                                                        'sm-offset-4' => 'sm-offset-4',
                                                        'sm-offset-5' => 'sm-offset-5',
                                                        'sm-offset-6' => 'sm-offset-6',
                                                        'sm-offset-7' => 'sm-offset-7',
                                                        'sm-offset-8' => 'sm-offset-8',
                                                        'sm-offset-9' => 'sm-offset-9',
                                                        'sm-offset-10' => 'sm-offset-10',
                                                        'sm-offset-11' => 'sm-offset-11',
                                                        'sm-offset-12' => 'sm-offset-12',
                                                        'md-offset-0' => 'md-offset-0',
                                                        'md-offset-1' => 'md-offset-1',
                                                        'md-offset-2' => 'md-offset-2',
                                                        'md-offset-3' => 'md-offset-3',
                                                        'md-offset-4' => 'md-offset-4',
                                                        'md-offset-5' => 'md-offset-5',
                                                        'md-offset-6' => 'md-offset-6',
                                                        'md-offset-7' => 'md-offset-7',
                                                        'md-offset-8' => 'md-offset-8',
                                                        'md-offset-9' => 'md-offset-9',
                                                        'md-offset-10' => 'md-offset-10',
                                                        'md-offset-11' => 'md-offset-11',
                                                        'md-offset-12' => 'md-offset-12',
                                                        'lg-offset-0' => 'lg-offset-0',
                                                        'lg-offset-1' => 'lg-offset-1',
                                                        'lg-offset-2' => 'lg-offset-2',
                                                        'lg-offset-3' => 'lg-offset-3',
                                                        'lg-offset-4' => 'lg-offset-4',
                                                        'lg-offset-5' => 'lg-offset-5',
                                                        'lg-offset-6' => 'lg-offset-6',
                                                        'lg-offset-7' => 'lg-offset-7',
                                                        'lg-offset-8' => 'lg-offset-8',
                                                        'lg-offset-9' => 'lg-offset-9',
                                                        'lg-offset-10' => 'lg-offset-10',
                                                        'lg-offset-11' => 'lg-offset-11',
                                                        'lg-offset-12' => 'lg-offset-12',
                                                    ),
                                                    'default_value' => array (
                                                        'xs-12' => 'xs-12',
                                                    ),
                                                    'allow_null' => 1,
                                                    'multiple' => 1,
                                                    'ui' => 1,
                                                    'ajax' => 1,
                                                    'placeholder' => '',
                                                    'disabled' => 0,
                                                    'readonly' => 0,
                                                ),
                                                array (
                                                    'key' => 'field_55412e00aa3ae',
                                                    'label' => 'Class',
                                                    'name' => 'text_class',
                                                    'type' => 'text',
                                                    'instructions' => '',
                                                    'required' => 0,
                                                    'conditional_logic' => 0,
                                                    'wrapper' => array (
                                                        'width' => '',
                                                        'class' => 'field_type-text field_key-field_55412e00aa3ae',
                                                        'id' => '',
                                                    ),
                                                    'default_value' => '',
                                                    'placeholder' => '',
                                                    'prepend' => '',
                                                    'append' => '',
                                                    'maxlength' => '',
                                                    'readonly' => 0,
                                                    'disabled' => 0,
                                                ),
                                                array (
                                                    'key' => 'field_55412e07aa3af',
                                                    'label' => 'Content',
                                                    'name' => 'wysiwyg_content',
                                                    'type' => 'wysiwyg',
                                                    'instructions' => '',
                                                    'required' => 0,
                                                    'conditional_logic' => 0,
                                                    'wrapper' => array (
                                                        'width' => '',
                                                        'class' => 'field_type-wysiwyg field_key-field_55412e07aa3af',
                                                        'id' => '',
                                                    ),
                                                    'default_value' => '',
                                                    'tabs' => 'all',
                                                    'toolbar' => 'full',
                                                    'media_upload' => 1,
                                                ),
                                            ),
                                        ),
                                    ),
                                    'min' => '',
                                    'max' => '',
                                ),
                            ),
                        ),
                    ),
                    'location' => $locations,
                    'menu_order' => 1,
                    'position' => 'normal',
                    'style' => 'default',
                    'label_placement' => 'top',
                    'instruction_placement' => 'label',
                    'hide_on_screen' => '',
                    'active' => 1,
                    'description' => '',
                ));
            }
        }


        /*
        *  content_filter
        *
        *  This function will add in some new parameters to the WP_Query args allowing fields to be found via key / name
        */

        public function contentFilter($content = '')
        {
            // Get Post
            global $post;
            $post_object = get_post($post->ID);
            setup_postdata($post_object);

            // Set layout
            $layout = '';

            // Set Option
            $option = ( is_home() ? "option" : '');

            // Set Flex Layout
            if (have_rows('flex_layout', $option)) {
                while (have_rows('flex_layout', $option)) {
                    the_row();
                    if (get_row_layout() == 'grid') {
                        $layout .= '<div id="'.get_sub_field('text_id').'" class="'.get_sub_field('text_class').'">';
                        $layout .= '<div class="'.get_sub_field('select_container').'">';
                        $layout .= '<div class="row grid-row">';
                        if (have_rows('repeater_grid')) {
                            while (have_rows('repeater_grid')) {
                                the_row();
                                $layout .= '<div class="grid-item '.(get_sub_field('select_columns') ? 'col-'.implode(' col-',get_sub_field('select_columns')) : '').' '.get_sub_field('text_class').'">'.get_sub_field('wysiwyg_content').'</div>';
                            }
                        }
                        $layout .= '</div></div></div>';
                    }
                }
            }
            return '<div class="site-container"><div class="site-row"><div class="site-col">'.$content.'</div></div></div>'. $layout;
        }
    }
}


/**
 * Instantiates the LayoutGrid
 */
function runLayoutGrid()
{
    $run = new LayoutGrid();
    $run->initialize();
}

runLayoutGrid();
